import React from 'react';
import {View, Text} from 'react-native';
import Index from '../index.ios.js';
import { action, observable, runInAction } from 'mobx';
import {CounterStore, Counter} from '../CounterComponent';
import {Provider} from 'mobx-react/custom';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('mobx-react/native', () => require('mobx-react/custom'));

const store = new CounterStore();

runInAction(() => {
  store.count = 2;
});

it('renders correctly', () => {
  const tree = renderer.create(
    <Provider counterStore={store}>
      <Counter/>
    </Provider>
  );
  console.log(tree.toJSON());
  tree.toJSON().children[0].props.onPress();
  expect(tree).toMatchSnapshot();
});
