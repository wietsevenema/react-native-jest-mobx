import React from 'react';
import {View, Text} from 'react-native';
import { action, observable, computed} from 'mobx';
import { Provider, inject, observer} from 'mobx-react/native';

class CounterComponent extends React.Component {
  render() {
    return (<View>
      <Text onPress={() => this.props.counterStore.increment()}>
        {this.props.counterStore.count}
        {this.props.counterStore.beThereOrBeSquare}
      </Text>
    </View>);
  }
}
export const Counter = inject('counterStore')(observer(CounterComponent));

export class CounterStore {
  @observable count = 0;

  @computed
  get beThereOrBeSquare() {
    return this.count * this.count;
  }

  @action
  increment() {
    this.count = this.count + 1;
  }
}
